import React, { Component } from 'react';
import TodoList from './component/TodoList';
import './App.css';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [
        {
          text: 'exsample todo',
          id: '1234'
        }
      ],
      noteText: ''
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(e) {
    this.setState({
      noteText: e.target.value
    });
  }
  handleSubmit(e) {
    e.preventDefault();
    if (!this.state.noteText.length) {
      return;
    }

    const NewItem = {
      text: this.state.noteText,
      id: Date.now()
    };

    this.setState(prevState => ({
      items: prevState.items.concat(NewItem),
      noteText: ''
    }));
  }

  render() {
    return (
      <div className="App">
        <div className="container">
          <div className="App-header">React Todo</div>
          <TodoList items={this.state.items} />
          <form>
            <input
              type="text"
              placeholder="Masukan Text? ..."
              ref={input => {
                this.textInput = input;
              }}
              className="textInput"
              value={this.state.noteText}
              onChange={this.handleChange}
            />
            <div className="btn" onClick={this.handleSubmit}>
              {' '}
              +{' '}
            </div>
          </form>
        </div>
      </div>
    );
  }
}
